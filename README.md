This repository use for topic on [Medium](https://medium.com/@darkbabybeau/https-medium-com-darkbabybeau-infrastructure-as-code-using-deployment-manager-382c4820fe90) that's about

## “Infrastructure as code using google cloud deployment manager to create the VM instance with docker service” ##

* The sourcecode based on "Google Cloud Platform".

* Easy to create VM instance and combine other applications.

* Reduce time for make the VM.

### P.S. Please read guideline on [this link](https://medium.com/@darkbabybeau/https-medium-com-darkbabybeau-infrastructure-as-code-using-deployment-manager-382c4820fe90) ###
