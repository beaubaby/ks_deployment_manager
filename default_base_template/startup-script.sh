#!/bin/bash
# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

file="/etc/first-init"

if [ -f "$file" ]
then
	echo "$file found."
else
    apt-get update
    apt-get install -y vim
    apt install -y python-pip
    apt install -y telnet

    ## Create a new disk and mount docker path to the new one ##
    systemctl stop docker
    rm -rf /var/lib/docker
    mkfs.ext4 -F /dev/sdb
    mkdir -p /var/lib/docker
    mount -o discard,defaults /dev/sdb /var/lib/docker
    echo UUID=`blkid -s UUID -o value /dev/sdb` /var/lib/docker ext4 discard,defaults,nofail 0 2 | tee -a /etc/fstab
    systemctl start docker
    systemctl enable docker

    ## Google authentication for docker
    gcloud auth configure-docker -q

    touch $file
fi